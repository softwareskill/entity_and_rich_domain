package pl.softwareskill.course.springdata.richdomain.encapsulated;

import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
interface EncapsulatedDepartmentRepository extends JpaRepository<EncapsulatedDepartment, Long> {

    boolean existsByNameIgnoreCase(String name);

    Optional<EncapsulatedDepartment> getByNameIgnoreCase(String name);
}
