package pl.softwareskill.course.springdata.richdomain.rich;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.softwareskill.course.springdata.richdomain.command.CreateDepartmentCommand;
import pl.softwareskill.course.springdata.richdomain.command.UpdateDepartmentCommand;
import pl.softwareskill.course.springdata.richdomain.dto.DepartmentDto;

@Entity
@Table(name = "DEPARTMENTS")
@Data //to jest nadmiarowe w przypadku gdy nie chcemy setterów/getterów
@NoArgsConstructor
class RichDomainDepartment {
    @Id
    @Column(name = "DEPARTMENT_ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DEPARTMENTS")
    Long departmentId;

    String name;

    @Embedded
    RichDomainAddress address;

    static RichDomainDepartment fromDto(CreateDepartmentCommand createDepartmentCommand) {
        var department = new RichDomainDepartment();
        department.setName(createDepartmentCommand.getName());
        department.setAddress(RichDomainAddress.fromDto(createDepartmentCommand.getAddress()));
        return department;
    }

    void update(UpdateDepartmentCommand updateDepartmentCommand) {
        this.name = updateDepartmentCommand.getName();
        this.address.update(updateDepartmentCommand.getAddress());
    }

    DepartmentDto toDto() {
        return DepartmentDto.builder()
                .name(name)
                .address(RichDomainAddress.toDto(address))
                .build();
    }
}
