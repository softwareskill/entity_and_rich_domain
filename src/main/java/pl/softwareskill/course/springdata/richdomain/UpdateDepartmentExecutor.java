package pl.softwareskill.course.springdata.richdomain;

import pl.softwareskill.course.springdata.richdomain.command.UpdateDepartmentCommand;
import pl.softwareskill.course.springdata.richdomain.dto.AddressDto;

class UpdateDepartmentExecutor {

    Department executeUpdate(Department department, UpdateDepartmentCommand updateDepartmentCommand) {
        department.setName(updateDepartmentCommand.getName());
        department.setAddress(toAddress(updateDepartmentCommand.getAddress()));
        return department;
    }

    private static Address toAddress(AddressDto addressDto) {
        Address address = new Address();
        address.setCity(addressDto.getCity());
        address.setCountry(addressDto.getCountry());
        address.setPostalCode(addressDto.getPostalCode());
        address.setStreet(addressDto.getStreet());
        address.setStreetNumber(addressDto.getStreetNumber());
        return address;
    }
}
