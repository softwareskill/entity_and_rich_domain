package pl.softwareskill.course.springdata.richdomain.rich;

import java.util.Optional;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import pl.softwareskill.course.springdata.richdomain.command.CreateDepartmentCommand;
import pl.softwareskill.course.springdata.richdomain.command.UpdateDepartmentCommand;
import pl.softwareskill.course.springdata.richdomain.dto.DepartmentCreationStatus;
import pl.softwareskill.course.springdata.richdomain.dto.DepartmentDto;
import pl.softwareskill.course.springdata.richdomain.dto.DepartmentId;
import pl.softwareskill.course.springdata.richdomain.dto.DepartmentUpdateStatus;

@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
@RequiredArgsConstructor
public class EncapsulatedDepartmentFacade {

    RichDepartmentRepository repository;

    public Optional<DepartmentDto> findDepartment(final DepartmentId departmentId) {
        return repository.findById(departmentId.getValue())
                .map(RichDomainDepartment::toDto);
    }

    public DepartmentCreationStatus createDepartment(final CreateDepartmentCommand createDepartmentCommand) {
        if (repository.existsByNameIgnoreCase(createDepartmentCommand.getName())) {
            return DepartmentCreationStatus.ALREADY_EXISTS;
        }
        var department = RichDomainDepartment.fromDto(createDepartmentCommand);
        repository.save(department);
        return DepartmentCreationStatus.CREATED;
    }

    public DepartmentUpdateStatus updateDepartment(final UpdateDepartmentCommand updateDepartmentCommand) {
        return repository.getByNameIgnoreCase(updateDepartmentCommand.getName())
                .map(department -> {
                    department.update(updateDepartmentCommand);
                    return DepartmentUpdateStatus.UPDATED;
                })
                .orElse(DepartmentUpdateStatus.DEPARTMENT_DOESNT_EXIST);
    }
}
