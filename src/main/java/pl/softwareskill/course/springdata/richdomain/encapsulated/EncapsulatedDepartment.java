package pl.softwareskill.course.springdata.richdomain.encapsulated;

import static java.util.Objects.isNull;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import pl.softwareskill.course.springdata.richdomain.command.CreateDepartmentCommand;
import pl.softwareskill.course.springdata.richdomain.command.UpdateDepartmentCommand;
import pl.softwareskill.course.springdata.richdomain.dto.AddressDto;
import pl.softwareskill.course.springdata.richdomain.dto.DepartmentDto;

@Entity
@Table(name = "DEPARTMENTS")
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
class EncapsulatedDepartment {
    @Id
    @Column(name = "DEPARTMENT_ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DEPARTMENTS")
    Long departmentId;

    String name;

    @Embedded
    EncapsulatedAddress address;

    static EncapsulatedDepartment fromDto(final CreateDepartmentCommand createDepartmentCommand) {
        var department = new EncapsulatedDepartment();
        department.name = createDepartmentCommand.getName();
        department.address = fromAddressDto(createDepartmentCommand.getAddress());
        return department;
    }

    private static EncapsulatedAddress fromAddressDto(final AddressDto addressDto) {
        if (isNull(addressDto) || addressDto.isEmpty()) {
            return null;
        }
        return EncapsulatedAddress.builder()
                .city(addressDto.getCity())
                .country(addressDto.getCountry())
                .postalCode(addressDto.getPostalCode())
                .street(addressDto.getStreet())
                .streetNumber(addressDto.getStreetNumber())
                .build();

    }

    //Gdy adres nie może istnieć bez obiektu opakowującego
    //enkapsulacja na poziomie Department
    void apply(UpdateDepartmentCommand updateDepartmentCommand) {
        this.name = updateDepartmentCommand.getName();
        var addressDto = updateDepartmentCommand.getAddress();
        updateAddress(this.address, addressDto);
    }

    private static void updateAddress(EncapsulatedAddress address, AddressDto addressDto) {
        address.setCity(addressDto.getCity());
        address.setCountry(addressDto.getCountry());
        address.setPostalCode(addressDto.getPostalCode());
        address.setStreet(addressDto.getStreet());
        address.setStreetNumber(addressDto.getStreetNumber());
    }

    DepartmentDto toDto() {
        return DepartmentDto.builder()
                .name(name)
                .address(toAddressDto(address))
                .build();
    }

    private static AddressDto toAddressDto(EncapsulatedAddress address) {
        if (isNull(address)) {
            return null;
        }
        return AddressDto.builder()
                .street(address.street)
                .streetNumber(address.streetNumber)
                .city(address.city)
                .country(address.country)
                .postalCode(address.postalCode)
                .build();
    }
}
