package pl.softwareskill.course.springdata.richdomain;

import static java.util.Objects.isNull;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.softwareskill.course.springdata.richdomain.dto.AddressDto;

@Embeddable
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder()
class Address {

    @Column(name = "STREET")
    String street;

    @Column(name = "STREET_NUMBER")
    String streetNumber;

    @Column(name = "CITY")
    String city;

    @Column(name = "COUNTRY")
    String country;

    @Column(name = "POSTAL_CODE")
    String postalCode;

    static Address fromDto(AddressDto addressDto) {
        if (isNull(addressDto) || addressDto.isEmpty()) {
            return null;
        }
        return Address.builder()
                .city(addressDto.getCity())
                .country(addressDto.getCountry())
                .postalCode(addressDto.getPostalCode())
                .street(addressDto.getStreet())
                .streetNumber(addressDto.getStreetNumber())
                .build();

    }

    static AddressDto toDto(Address address) {
        if (isNull(address)) {
            return null;
        }
        return AddressDto.builder()
                .street(address.street)
                .streetNumber(address.streetNumber)
                .city(address.city)
                .country(address.country)
                .postalCode(address.postalCode)
                .build();
    }

    void update(AddressDto address) {
        this.city = address.getCity();
        this.country = address.getCountry();
        this.postalCode = address.getPostalCode();
        this.street = address.getStreet();
        this.streetNumber = address.getStreetNumber();
    }
}
