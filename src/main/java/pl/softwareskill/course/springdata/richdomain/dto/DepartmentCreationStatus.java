package pl.softwareskill.course.springdata.richdomain.dto;

public enum DepartmentCreationStatus {
    CREATED,
    ALREADY_EXISTS,
    IN_PROGRESS,
    INVALID_INPUT
}
