package pl.softwareskill.course.springdata.richdomain.dto;

public enum DepartmentUpdateStatus {
    UPDATED,
    DEPARTMENT_DOESNT_EXIST
}
