package pl.softwareskill.course.springdata.richdomain.encapsulated;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Embeddable
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder()
class EncapsulatedAddress {

    @Column(name = "STREET")
    String street;

    @Column(name = "STREET_NUMBER")
    String streetNumber;

    @Column(name = "CITY")
    String city;

    @Column(name = "COUNTRY")
    String country;

    @Column(name = "POSTAL_CODE")
    String postalCode;

}
