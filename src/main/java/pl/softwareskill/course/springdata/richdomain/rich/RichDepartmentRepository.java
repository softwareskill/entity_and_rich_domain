package pl.softwareskill.course.springdata.richdomain.rich;

import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
interface RichDepartmentRepository extends JpaRepository<RichDomainDepartment, Long> {

    boolean existsByNameIgnoreCase(String name);

    Optional<RichDomainDepartment> getByNameIgnoreCase(String name);
}
