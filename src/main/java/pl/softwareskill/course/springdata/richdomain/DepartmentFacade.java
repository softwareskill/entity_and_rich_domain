package pl.softwareskill.course.springdata.richdomain;

import java.util.Optional;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import pl.softwareskill.course.springdata.richdomain.command.CreateDepartmentCommand;
import pl.softwareskill.course.springdata.richdomain.command.UpdateDepartmentCommand;
import pl.softwareskill.course.springdata.richdomain.dto.DepartmentCreationStatus;
import pl.softwareskill.course.springdata.richdomain.dto.DepartmentDto;
import pl.softwareskill.course.springdata.richdomain.dto.DepartmentId;
import pl.softwareskill.course.springdata.richdomain.dto.DepartmentUpdateStatus;

@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
@RequiredArgsConstructor
public class DepartmentFacade {

    DepartmentRepository repository;

    public Optional<DepartmentDto> findDepartment(final DepartmentId departmentId) {
        return repository.findById(departmentId.getValue())
                .map(department -> new DepartmentMapper().toDto(department));
    }

    public DepartmentCreationStatus createDepartment(final CreateDepartmentCommand createDepartmentCommand) {
        if (repository.existsByNameIgnoreCase(createDepartmentCommand.getName())) {
            return DepartmentCreationStatus.ALREADY_EXISTS;
        }
        var department = DepartmentMapper.fromDto(createDepartmentCommand);
        repository.save(department);
        return DepartmentCreationStatus.CREATED;
    }

    public DepartmentUpdateStatus updateDepartment(final UpdateDepartmentCommand updateDepartmentCommand) {
        return repository.getByNameIgnoreCase(updateDepartmentCommand.getName())
                .map(department -> {
                    new UpdateDepartmentExecutor().executeUpdate(department, updateDepartmentCommand);
                    return DepartmentUpdateStatus.UPDATED;
                })
                .orElse(DepartmentUpdateStatus.DEPARTMENT_DOESNT_EXIST);
    }
}
