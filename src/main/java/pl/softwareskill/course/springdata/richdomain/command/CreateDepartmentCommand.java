package pl.softwareskill.course.springdata.richdomain.command;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import static com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.ANY;
import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.experimental.FieldDefaults;
import pl.softwareskill.course.springdata.richdomain.dto.AddressDto;

@Getter
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
@Builder
@JsonAutoDetect(fieldVisibility = ANY)
public class CreateDepartmentCommand {
    String name;

    AddressDto address;

    @JsonCreator(mode = JsonCreator.Mode.PROPERTIES)
    @SuppressWarnings("unused")
    public CreateDepartmentCommand(String name, AddressDto address) {
        this.name = name;
        this.address = address;
    }
}
