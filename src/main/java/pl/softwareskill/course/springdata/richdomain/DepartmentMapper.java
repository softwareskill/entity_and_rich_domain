package pl.softwareskill.course.springdata.richdomain;

import static java.util.Objects.isNull;
import pl.softwareskill.course.springdata.richdomain.command.CreateDepartmentCommand;
import pl.softwareskill.course.springdata.richdomain.dto.AddressDto;
import pl.softwareskill.course.springdata.richdomain.dto.DepartmentDto;

class DepartmentMapper {

    DepartmentDto toDto(final Department department) {
        return DepartmentDto.builder()
                .name(department.getName())
                .address(toAddressDto(department.getAddress()))
                .build();
    }

    private static AddressDto toAddressDto(Address address) {
        if (isNull(address)) {
            return null;
        }
        return AddressDto.builder()
                .street(address.street)
                .streetNumber(address.streetNumber)
                .city(address.city)
                .country(address.country)
                .postalCode(address.postalCode)
                .build();
    }

    static Department fromDto(CreateDepartmentCommand createDepartmentCommand) {
        var department = new Department();
        department.setName(createDepartmentCommand.getName());
        department.setAddress(fromAddressDto(createDepartmentCommand.getAddress()));
        return department;
    }

    private static Address fromAddressDto(AddressDto addressDto) {
        if (isNull(addressDto) || addressDto.isEmpty()) {
            return null;
        }
        return Address.builder()
                .city(addressDto.getCity())
                .country(addressDto.getCountry())
                .postalCode(addressDto.getPostalCode())
                .street(addressDto.getStreet())
                .streetNumber(addressDto.getStreetNumber())
                .build();

    }
}
